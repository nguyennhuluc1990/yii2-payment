<?php
/**
 * @link https://github.com/nguyennhuluc1990/yii2-payment
 * @copyright Copyright (c) 2017 Yii Viet
 * @license [New BSD License](http://www.opensource.org/licenses/bsd-license.php)
 */

return [
    'class' => 'nguyennhuluc1990\payment\onepay\PaymentClient',
    'merchantId' => 'TESTONEPAY',
    'accessCode' => '6BEB2546',
    'secureSecret' => '6D0870CDE5F24F34F3915FB0045120DB'
];
